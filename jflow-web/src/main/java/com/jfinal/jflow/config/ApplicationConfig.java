/**
 * 
 */
package com.jfinal.jflow.config;

import java.sql.Connection;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.FakeStaticHandler;
import com.jfinal.ext.proxy.CglibProxyFactory;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.AnsiSqlDialect;
import com.jfinal.plugin.activerecord.dialect.Dialect;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.dialect.OracleDialect;
import com.jfinal.plugin.activerecord.dialect.PostgreSqlDialect;
import com.jfinal.plugin.activerecord.dialect.SqlServerDialect;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.template.Engine;

import BP.DA.DBType;
import BP.Difference.JFinalUtils;
import BP.Difference.RequestInterceptor;
import BP.Difference.Handler.AttachmentUploadController;
import BP.Difference.Handler.CCBPMDesignerController;
import BP.Difference.Handler.IndexController;
import BP.Difference.Handler.SFTableHandler_Controller;
import BP.Difference.Handler.WF_Comm_Controller;
import BP.Tools.DateUtils;
import Controller.RestFulController;

/**
 * 应用配置
 * 
 * @author 韩世强
 */
public class ApplicationConfig extends JFinalConfig {

	static Prop prop;
	
	@Override
	public void configConstant(Constants me) {
		loadConfig();
		
		int isDebug = prop.getInt("IsDebug", 0);
		me.setDevMode(isDebug == 1);
		me.setInjectDependency(true);
		me.setJsonDatePattern(DateUtils.YEAR_MONTH_DAY_PATTERN_MIDLINE);
		
		// 使用 CglibProxyFactory 做代理，支持在 JRE 下部署
		me.setProxyFactory(new CglibProxyFactory());
	}

	@Override
	public void configRoute(Routes me) {
		me.add("/", IndexController.class);	
		me.add("/WF/Comm", WF_Comm_Controller.class);
		me.add("/WF/Admin/CCBPMDesigner", CCBPMDesignerController.class);
		me.add("/DataUser/SFTableHandler", SFTableHandler_Controller.class);
		me.add("/WF/Ath", AttachmentUploadController.class);
		me.add("/restful", RestFulController.class);
	}

	@Override
	public void configEngine(Engine me) {
	}

	@Override
	public void configPlugin(Plugins me) {
		loadConfig();
		
		DruidPlugin druidPlugin = JFinalUtils.getDruidPlugin();
		me.add(druidPlugin);
		
		ActiveRecordPlugin activeRecordPlugin = new ActiveRecordPlugin(druidPlugin);
		//配置事务级别为读已提交
		activeRecordPlugin.setTransactionLevel(Connection.TRANSACTION_READ_COMMITTED);
		//配置数据库方言
		activeRecordPlugin.setDialect(getDataSourceDialect());

		me.add(activeRecordPlugin);
	}

	@Override
	public void configInterceptor(Interceptors me) {
		me.addGlobalActionInterceptor(new RequestInterceptor());
	}

	@Override
	public void configHandler(Handlers me) {
		me.add(new StaticHandler());
		me.add(new FakeStaticHandler(".do"));
	}
	
	/**
	 * 加载配置文件
	 */
	private static void loadConfig() {
		if(prop == null) {
			prop = PropKit.use("jflow.properties");
		}
	}
	
	/**
	 * 获取配置的数据库的方言
	 * @return
	 */
	private Dialect getDataSourceDialect() {
		DBType dataBaseType = BP.Difference.SystemConfig.getAppCenterDBType();
		if(dataBaseType == DBType.MySQL) {
			return new MysqlDialect();
		}else if(dataBaseType == DBType.MSSQL) {
			return new SqlServerDialect();
		}else if(dataBaseType == DBType.Oracle) {
			return new OracleDialect();
		}else if(dataBaseType == DBType.PostgreSQL) {
			return new PostgreSqlDialect();
		}
		
		return new AnsiSqlDialect();
	}
}
