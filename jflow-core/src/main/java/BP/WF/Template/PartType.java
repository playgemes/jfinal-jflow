package BP.WF.Template;

import BP.DA.*;
import BP.En.*;
import BP.WF.Template.*;
import BP.Port.*;
import BP.WF.*;
import java.util.*;

/** 
 配件类型
*/
public class PartType
{
	/** 
	 前置导航的父子流程关系
	*/
	public static final String ParentSubFlowGuide = "ParentSubFlowGuide";
	/** 
	 流程时限设置
	*/
	public static final String DeadLineRole = "DeadLineRole";
}