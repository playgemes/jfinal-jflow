package BP.WF.Rpt;

import BP.DA.*;
import BP.En.*;
import BP.Port.*;
import BP.WF.*;
import java.util.*;

/** 
 报表岗位
*/
public class RptStationAttr
{

		///#region 基本属性
	/** 
	 报表ID
	*/
	public static final String FK_Rpt = "FK_Rpt";
	/** 
	 岗位
	*/
	public static final String FK_Station = "FK_Station";

		///#endregion
}