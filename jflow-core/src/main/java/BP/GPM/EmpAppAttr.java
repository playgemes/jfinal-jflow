package BP.GPM;

import BP.DA.*;
import BP.Web.*;
import BP.En.*;
import java.util.*;

/** 
 管理员与系统权限
*/
public class EmpAppAttr
{
	/** 
	 操作员
	*/
	public static final String FK_Emp = "FK_Emp";
	/** 
	 系统
	*/
	public static final String FK_App = "FK_App";
}