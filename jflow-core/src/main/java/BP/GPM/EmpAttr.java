package BP.GPM;

import BP.DA.*;
import BP.En.*;
import BP.Port.*;
import java.util.*;

/** 
 操作员属性
*/
public class EmpAttr extends BP.En.EntityNoNameAttr
{

		///#region 基本属性
	/** 
	 部门
	*/
	public static final String FK_Dept = "FK_Dept";
	/** 
	 密码
	*/
	public static final String Pass = "Pass";
	/** 
	 sid
	*/
	public static final String SID = "SID";
	/** 
	 电话
	*/
	public static final String Tel = "Tel";
	/** 
	 邮箱
	*/
	public static final String Email = "Email";
	/** 
	 序号
	*/
	public static final String Idx = "Idx";
	/** 
	 拼音
	*/
	public static final String PinYin = "PinYin";

		///#endregion
	/** 
	 签字类型
	*/
	public static final String SignType = "SignType";
}